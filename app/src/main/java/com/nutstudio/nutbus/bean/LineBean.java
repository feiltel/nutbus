package com.nutstudio.nutbus.bean;

/**
 * Created by Administrator on 2017/1/18 0018.
 */

public class LineBean {
    public int getCarNum() {
        return carNum;
    }

    public void setCarNum(int carNum) {
        this.carNum = carNum;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    private String stationName;
    private int carNum;
    private int statinId;

    public int getStatinId() {
        return statinId;
    }

    public void setStatinId(int statinId) {
        this.statinId = statinId;
    }
}
