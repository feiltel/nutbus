package com.nutstudio.nutbus.utils.request;

import org.json.JSONObject;

public interface IRequestEvent {
    void responseSuccess(JSONObject jsonObject, int whichCall);
    void responseSuccess(String str, int whichCall);
    void responseError(int whichCall);
}